var {expect} = require('chai')
var {isRealString} = require('./../utils/validation')

describe('string validatin', () => {
    it('should reject non-string vlues', () => {
        var str = 1233123123
        expect(isRealString(str)).to.be.false
    })

    it('should reject only spaces string', () => {
        var str = '         '
        expect(isRealString(str)).to.be.false
    })

    it('should return true if string is correct', () =>{
        var str = 'sdasd'
        expect(isRealString(str)).to.be.true
    })
})