var {generateMessage, generateLocationMessage} = require('../utils/messages')
var {expect} = require('chai')

describe('generateMessage', () => {
    it('should generate a message', () =>{
        var from = 'sgsegsgd'
        var text = 'fasfsddhgfn'
        message = generateMessage(from, text)
        expect(message).to.include.keys('from', 'text', 'createdAt')
        expect(message.from).to.be.a('string')
        expect(message.text).to.be.a('string')
    })

    it('should generate a location message', () => {
        var from = 'usertest'
        var lat = 51.20
        var lng = -60.91
        message = generateLocationMessage(from, lat, lng)
        expect(message).to.include.keys('from', 'createdAt', 'url')
        expect(message.url).to.equals(`https://www.google.com/maps?q=${lat},${lng}`)
    })
})