var {expect} = require('chai')
var {Users} = require('./../utils/users')

describe('Users tests', () => {
    var users = new Users()
    beforeEach(() => {
        users.users = [{
            id: 1,
            name: 'name1',
            room: 'room1'
        }, {
            id: 2,
            name: 'name2',
            room: 'room2'
        }]
    })
    it('should add new user', () => {
        var usr = {id: 1, name: 'nnn', room: 'rrr'}
        var count = users.users.length
        expect(users.addUser(1, 'nnn', 'rrr')).to.eql(usr)
        expect(users.users.length).to.be.greaterThan(count)
    })

    it('should find a user', () => {
        var id = 1
        expect(users.getUser(id)).to.eql({id: 1, name: 'name1', room: 'room1'})
    })

    it('should not find a user', () => {
        var id = 99999
        expect(users.getUser(id)).to.be.undefined
    })

    it('should not remove a user', () => {
        var count = users.users.length
        expect(users.removeUser(1000)).to.be.undefined
        expect(users.users.length).to.eql(count)
    })

    it('should remove a user', () => {
        var count = users.users.length
        expect(users.removeUser(2)).to.eql({id: 2, name: 'name2', room: 'room2'})
        expect(users.users.length).to.be.lessThan(count)
    })

    it('should return names array', () => {
        var names = users.getUsersList('room1')
        expect(names).to.be.an('array')
        expect(names).to.include('name1')
    })
})