const port = process.env.PORT || 3000

var http = require('http')
var path = require('path')
var express = require('express')
var socketIO = require('socket.io')
var {isRealString} = require('./utils/validation')
var {Users} = require('./utils/users')

var {generateMessage, generateLocationMessage} = require('./utils/messages')
var publicPath = path.join(__dirname + '/../public')
var app = express()
var server = http.createServer(app)
var io = socketIO(server)
var users = new Users()
app.use(express.static(publicPath))

io.on('connection', (socket) => {
    console.log('New user connected')

    

    socket.on('createLocationMessage', (coords) => {
        io.emit('newLocationMessage', generateLocationMessage('User', coords.lat, coords.lng))
    })

    socket.on('join', (params, callback) => {
        if(!isRealString(params.name) || !isRealString(params.room)){
            return callback('Name and room are required')
        }

        socket.join(params.room)
        users.removeUser(socket.id)
        users.addUser(socket.id, params.name, params.room)

        io.to(params.room).emit('updateUsersList', users.getUsersList(params.room))

        socket.emit('newMessage', generateMessage('Admin', 'Weclome to my app'))

        socket.broadcast.to(params.room).emit('newMessage', generateMessage('Admin', `${params.name} has connected`))
        
        socket.on('disconnect', () => {
            console.log('user disconnect')
            users.removeUser(socket.id)
            io.to(params.room).emit('newMessage', generateMessage('Admin', `${params.name} has disconnected`))
            io.to(params.room).emit('updateUsersList', users.getUsersList(params.room))
        })

        socket.on('createMessage', (newMessage, callback) => {
            console.log(newMessage)
            io.to(params.room).emit('newMessage', generateMessage(newMessage.from, newMessage.text))
            callback()
        })

        callback()
    })

})

server.listen(port, () => {
    console.log(`Listen on port ${port}`)
})