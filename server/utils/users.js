class Users{
    constructor(){
        this.users = []
    }

    addUser(id, name, room){
        var user = {id, name, room}
        this.users.push(user)
        return user
    }

    removeUser(id){
        var user = this.getUser(id)
        this.users = this.users.filter((usr) => usr.id !== id)

        return user
    }

    getUser(id){
        var user = this.users.filter((usr) => usr.id === id)[0]
        return user
    }

    getUsersList(room){
        var users = this.users.filter((usr) => {
            return usr.room === room
        })

        var names = users.map((usr) => usr.name)
        return names
    }
}

module.exports = {Users}