var moment = require('moment')
var generateMessage = (from, text) => {
    var time = moment().valueOf()
    return {
        from,
        text,
        createdAt: time
    }
}

var generateLocationMessage = (from, lat, lng) => {
    var time = moment().valueOf()
    return {
        from,
        url: `https://www.google.com/maps?q=${lat},${lng}`,
        createdAt: time
    }
}
module.exports = {generateMessage, generateLocationMessage}