var socket = io()

function scrollToBottom(){

    var messages = jQuery('#messages')
    var clientHeight = messages.prop('clientHeight')
    var scrollTop = messages.prop('scrollTop')
    var scrollHeight = messages.prop('scrollHeight')
}
socket.on('connect',  function(){
    var params = jQuery.deparam(window.location.search)
    socket.emit('join', params, function(err){
        if (err){
            alert(err)
            window.location.href = '/'
        } else{
            console.log('No errors')
        }
    })
})

socket.on('disconnect', function (){
    console.log('Disconnected from server')
})

socket.on('updateUsersList', function(users){
    var ol = jQuery('<ol></ol>')

    users.forEach(usr => {
        ol.append(jQuery('<li></li>').text(usr))
    })

    jQuery('#users').html(ol)
})

socket.on('newUserConnected', function(){
    console.log('New user connected')
})

socket.on('newLocationMessage', function (newLocationMessage){
    var message_time = moment(newLocationMessage.createdAt).format('h:mm:ss a')
    var li = jQuery('<li></li>')
    var a = jQuery('<a target="_blank">My location</a>')
    a.attr('href', newLocationMessage.url)
    
    li.text(`${newLocationMessage.from} ${message_time}: `)
    li.append(a)
    jQuery('#messages').append(li)
    scrollToBottom()
})


socket.on('newMessage', function(newMessage){
    var message_time = moment(newMessage.createdAt).format('h:mm:ss a')
    var template = jQuery('#message-template').html()
    var html = Mustache.render(template, {
        text: newMessage.text,
        from: newMessage.from,
        createdAt: message_time
    })

    jQuery('#messages').append(html)
    scrollToBottom()
})


jQuery('#message-form').on('submit', function(e){
    e.preventDefault()
    var params = jQuery.deparam(window.location.search)

    socket.emit('createMessage', {
        from: params.name,
        text: jQuery('[name=message').val()
    }, function(){
        jQuery('[name=message]').val('')
    })
})

var locationButton = jQuery("#location-button")
locationButton.on('click', function(){
    if(!navigator.geolocation){
        return alert("Your browser doesn't support geolocation")
    }

    navigator.geolocation.getCurrentPosition(function (position){
        socket.emit('createLocationMessage', {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        })
    }, function(){
        alert('Unable to get geolocation')
    })
})